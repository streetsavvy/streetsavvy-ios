//
//  ViewController.swift
//  StreetSavvy
//
//  Created by Nicholas Kachur on 4/27/18.
//  Copyright © 2018 Nicholas Kachur. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // Constants
    let initialRadius: CLLocationDistance = 10000
    let initialLocation = CLLocationCoordinate2D(latitude: 37.7656, longitude: -122.4115)
    
    // Variables
    var fromPlacemark: MKPlacemark? = nil
    var toPlacemark: MKPlacemark? = nil
    var safeRoutePolyline: MKPolyline? = nil
    var shortRoutePolyline: MKPolyline? = nil
    var heatmapOverlay: HeatmapOverlay? = nil
    var apiService = StreetSavvyAPIService()

// MARK - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        centerMapOnLocation(coordinate: initialLocation, span: 0.1)
        self.apiService.getStreetSavvyCheckEndpoint() { results, errorMessage in
            print("DEBUG: API check results: \(results)")
        }
        
        let bottomRight = MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 37.7007, longitude: -122.5175))
        let topLeft = MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 37.8204, longitude: -122.3506))
        let overlayBoundingMapRect = MKMapRectMake(
            topLeft.x,
            topLeft.y,
            -fabs(topLeft.x - bottomRight.x),
            fabs(topLeft.y - bottomRight.y)
        )
        heatmapOverlay = HeatmapOverlay(boundingMapRect: overlayBoundingMapRect, midCoordinate: initialLocation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

// MARK - Utility Functions
    func centerMapOnLocation(coordinate: CLLocationCoordinate2D, span: Double) {
        let mkSpan = MKCoordinateSpanMake(span, span)
        let coordinateRegion = MKCoordinateRegionMake(coordinate, mkSpan)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
// MARK - IBActions
    @IBAction func fromTextFieldEditingDidEnd(_ sender: UITextField) {
        guard let searchString = sender.text else {
            return
        }
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchString
        request.region = mapView.region
        
        if fromPlacemark != nil {
            mapView.removeAnnotation(fromPlacemark!)
        }
        if shortRoutePolyline != nil {
            mapView.remove(shortRoutePolyline!)
        }
        if safeRoutePolyline != nil {
            mapView.remove(safeRoutePolyline!)
        }
        
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: { (response, error) in
            guard let response = response else {
                print("ERROR: couldn't find \(request.naturalLanguageQuery!): \(error!)")
                return
            }
            
            self.fromPlacemark = response.mapItems[0].placemark
            self.mapView.addAnnotation(self.fromPlacemark!)
        })
        
        print("DEBUG: Found coordinate \(String(describing: self.fromPlacemark?.coordinate)) for search \(String(describing: request.naturalLanguageQuery))")
        sender.resignFirstResponder()
    }
    
    @IBAction func toTextFieldEditingDidEnd(_ sender: UITextField) {
        guard let searchString = sender.text else {
            return
        }
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchString
        request.region = mapView.region
        
        if toPlacemark != nil {
            mapView.removeAnnotation(toPlacemark!)
        }
        if shortRoutePolyline != nil {
            mapView.remove(shortRoutePolyline!)
        }
        if safeRoutePolyline != nil {
            mapView.remove(safeRoutePolyline!)
        }
        
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: { (response, error) in
            guard let response = response else {
                print("ERROR: couldn't find \(request.naturalLanguageQuery!): \(error!)")
                return
            }
            
            self.toPlacemark = response.mapItems[0].placemark
            self.mapView.addAnnotation(self.toPlacemark!)
        })
        
        print("DEBUG: Found coordinate \(String(describing: self.toPlacemark?.coordinate)) for search \(String(describing: request.naturalLanguageQuery))")
        sender.resignFirstResponder()
    }
    
    @IBAction func safeRouteButtonTouchUpInside(_ sender: UIButton) {
        if fromPlacemark != nil && toPlacemark != nil {
            apiService.getStreetSavvySafeRoute(fromCoordinate: (toPlacemark?.coordinate)!, toCoordinate: (fromPlacemark?.coordinate)!)
            { results, errorMessage in
                print("DEBUG: safeRouteButtonTouchUpInside results: \(results)")
                guard let path = results.path else {
                    return
                }
                var coordinates: [CLLocationCoordinate2D] = []
                for i in 0..<path.count {
                    coordinates.append(CLLocationCoordinate2D(latitude: path[i][1], longitude: path[i][0]))
                }
                self.safeRoutePolyline = SafeRoutePolyline(coordinates: coordinates, count: coordinates.count)
                self.mapView.add(self.safeRoutePolyline!)
            }
        }
    }
    
    @IBAction func shortRouteButtonTouchUpInside(_ sender: UIButton) {
        if fromPlacemark != nil && toPlacemark != nil {
            apiService.getStreetSavvyDistanceRoute(fromCoordinate: (toPlacemark?.coordinate)!, toCoordinate: (fromPlacemark?.coordinate)!)
            { results, errorMessage in
                print("DEBUG: shortRouteButtonTouchUpInside results: \(results)")
                guard let path = results.path else {
                    return
                }
                var coordinates: [CLLocationCoordinate2D] = []
                for i in 0..<path.count {
                    coordinates.append(CLLocationCoordinate2D(latitude: path[i][1], longitude: path[i][0]))
                }
                self.shortRoutePolyline = ShortRoutePolyline(coordinates: coordinates, count: coordinates.count)
                self.mapView.add(self.shortRoutePolyline!)
            }
        }
    }
    
    @IBAction func heatmapSwitchValueChanged(_ sender: UISwitch) {
        if sender.isOn {
            mapView.add(heatmapOverlay!, level: MKOverlayLevel.aboveRoads)
        } else {
            mapView.remove(heatmapOverlay!)
        }
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is HeatmapOverlay {
            return HeatmapOverlayView(overlay: overlay, overlayImage: #imageLiteral(resourceName: "heatmap"))
        } else if overlay is SafeRoutePolyline {
            let lineView = MKPolylineRenderer(overlay: overlay)
            lineView.strokeColor = UIColor.cyan.withAlphaComponent(0.8)
            return lineView
        } else if overlay is ShortRoutePolyline {
            let lineView = MKPolylineRenderer(overlay: overlay)
            lineView.strokeColor = UIColor.blue.withAlphaComponent(0.8)
            return lineView
        }
        
        return MKOverlayRenderer()
    }
}
