//
//  HeatmapOverlay.swift
//  StreetSavvy
//
//  Created by Nicholas Kachur on 5/23/18.
//  Copyright © 2018 Nicholas Kachur. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class HeatmapOverlay: NSObject, MKOverlay {
    var coordinate: CLLocationCoordinate2D
    var boundingMapRect: MKMapRect
    
    init(boundingMapRect: MKMapRect, midCoordinate: CLLocationCoordinate2D) {
        self.boundingMapRect = boundingMapRect
        self.coordinate = midCoordinate
    }
}

class HeatmapOverlayView: MKOverlayRenderer {
    var overlayImage: UIImage
    
    init(overlay: MKOverlay, overlayImage: UIImage) {
        self.overlayImage = overlayImage
        super.init(overlay: overlay)
    }
    
    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
        guard let imageReference = overlayImage.cgImage else { return }
        
        let rect = self.rect(for: overlay.boundingMapRect)
        context.setAlpha(0.5)
        context.scaleBy(x: 1.0, y: -1.0)
        context.translateBy(x: 0.0, y: -rect.size.height)
        context.draw(imageReference, in: rect)
    }
}
