//
//  SSHeatmap.swift
//  StreetSavvy
//
//  Created by Nicholas Kachur on 5/23/18.
//  Copyright © 2018 Nicholas Kachur. All rights reserved.
//

import Foundation
import MapKit

class Heatmap {
    var name: String?
    var boundary: [CLLocationCoordinate2D] = []
    
    var midCoordinate = CLLocationCoordinate2D()
    var overlayTopLeftCoordinate = CLLocationCoordinate2D
}
