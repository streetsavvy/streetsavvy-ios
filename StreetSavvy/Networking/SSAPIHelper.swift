//
//  StreetSavvyAPIHelper.swift
//  StreetSavvy
//
//  Created by Nicholas Kachur on 5/14/18.
//  Copyright © 2018 Nicholas Kachur. All rights reserved.
//

import Foundation
import MapKit

struct Check: Decodable {
    let Running: Bool?
}

struct Route: Decodable {
    let path: [SSAPICoordinate]?
    let error: String?
}

typealias SSAPICoordinate = [Double]
typealias CheckResponse = [Check]
typealias CheckResult = (CheckResponse, String) -> ()
typealias RouteResult = (Route, String) -> ()



class StreetSavvyAPIService {

    
    let defaultSession = URLSession(configuration: .default)
    var checkTask: URLSessionDataTask?
    var safetyRouteTask: URLSessionDataTask?
    var distanceRouteTask: URLSessionDataTask?
    var heatmapTask: URLSessionDownloadTask?
    let decoder = JSONDecoder()
    var errorMessage = ""
    var data: String = ""

    func getStreetSavvyCheckEndpoint(completion: @escaping CheckResult) {
        checkTask?.cancel()
        let url = URL(string: "https://dev.streetsavvy.io/api/check")!
        checkTask = defaultSession.dataTask(with: url) { data, response, error in
            defer { self.checkTask = nil }
            if let error = error {
                print("Error perfoming getStreetSavvyCheckEndpoint: \(error.localizedDescription).")
                self.errorMessage = "DataTask error performing getStreetSavvyCheckEndpoint: \(error.localizedDescription)\n"
                return
            } else if let data = data,
                      let response = response as? HTTPURLResponse,
                      response.statusCode == 200 {
                do {
                    let jsonData = try self.decoder.decode(CheckResponse.self, from: data)
                    DispatchQueue.main.async {
                        completion(jsonData, self.errorMessage)
                    }
                } catch {
                    print("error decoding JSON: \(error.localizedDescription)")
                }
            }
        }
        checkTask?.resume()
    }
    
    func getStreetSavvySafeRoute(fromCoordinate: CLLocationCoordinate2D, toCoordinate: CLLocationCoordinate2D, completion: @escaping RouteResult) {
        safetyRouteTask?.cancel()
        if var urlComponents = URLComponents(string: "https://dev.streetsavvy.io/api/risk_route") {
            urlComponents.query = "lat1=\(fromCoordinate.latitude)&lon1=\(fromCoordinate.longitude)&lat2=\(toCoordinate.latitude)&lon2=\(toCoordinate.longitude)"
            guard let url = urlComponents.url else {
                print("ERROR: couldn't perform getStreetSavvySafeRoute: invalid url")
                return
            }
            print("DEBUG: getStreetSavvySafeRoute: url is: \(url)")
            
            safetyRouteTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.safetyRouteTask = nil }
                if let error = error {
                    print("ERROR: couldn't perform getStreetSavvySafeRoute: \(error.localizedDescription).")
                    self.errorMessage = "ERROR: couldn't perform getStreetSavvySafeRoute: \(error.localizedDescription)\n"
                    return
                } else if let data = data,
                          let response = response as? HTTPURLResponse,
                          (200...299).contains(response.statusCode) {
                    print("DEBUG: getStreetSavvySafeRoute: HTTP GET successful")
                    do {
                        let jsonData = try self.decoder.decode(Route.self, from: data)
                        DispatchQueue.main.async {
                            completion(jsonData, self.errorMessage)
                        }
                    } catch {
                        print("ERROR: couldn't decode JSON: \(error)")
                    }
                } else if let response = response as? HTTPURLResponse {
                    print("DEBUG: HTTP response was: \(response.statusCode)")
                }
            }
            
            safetyRouteTask?.resume()
        }
    }
    
    func getStreetSavvyDistanceRoute(fromCoordinate: CLLocationCoordinate2D, toCoordinate: CLLocationCoordinate2D, completion: @escaping RouteResult) {
        distanceRouteTask?.cancel()
        if var urlComponents = URLComponents(string: "https://dev.streetsavvy.io/api/route") {
            urlComponents.query = "lat1=\(fromCoordinate.latitude)&lon1=\(fromCoordinate.longitude)&lat2=\(toCoordinate.latitude)&lon2=\(toCoordinate.longitude)"
            guard let url = urlComponents.url else {
                print("ERROR: couldn't perform getStreetSavvySafeRoute: invalid url")
                return
            }
            print("DEBUG: getStreetSavvySafeRoute: url is: \(url)")
            
            distanceRouteTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.distanceRouteTask = nil }
                if let error = error {
                    print("ERROR: couldn't perform getStreetSavvySafeRoute: \(error.localizedDescription).")
                    self.errorMessage = "ERROR: couldn't perform getStreetSavvySafeRoute: \(error.localizedDescription)\n"
                    return
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    (200...299).contains(response.statusCode) {
                    print("DEBUG: getStreetSavvySafeRoute: HTTP GET successful")
                    do {
                        let jsonData = try self.decoder.decode(Route.self, from: data)
                        DispatchQueue.main.async {
                            completion(jsonData, self.errorMessage)
                        }
                    } catch {
                        print("ERROR: couldn't decode JSON: \(error)")
                    }
                } else if let response = response as? HTTPURLResponse {
                    print("DEBUG: HTTP response was: \(response.statusCode)")
                }
            }
            
            distanceRouteTask?.resume()
        }
    }
}
